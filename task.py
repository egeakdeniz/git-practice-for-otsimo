import PyPDF2
import re

def find_email_in_pdf(fileobj):
    pdf = PyPDF2.PdfFileReader(fileobj)
    number_of_pages = pdf.getNumPages()
    string = "@"
    result = ''
    for i in range(0, number_of_pages):
        pg = pdf.getPage(i)
        txt = pg.extractText()
        result = result + txt
    return result

f = "resume-2.pdf"
print(find_email_in_pdf(f))
